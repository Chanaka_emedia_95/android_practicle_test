package com.example.testproject

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.testproject.extendtions.setValue
import com.example.testproject.extendtions.startActivity
import com.example.testproject.extendtions.toJsonString
import com.example.testproject.model.User
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val sharedPreferences: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        save.setOnClickListener {
            proceedSignIn()
        }

    }


    private fun proceedSignIn() {
        val gender = if (radioButton_female.isChecked) {
            "female"
        } else {
            "male"
        }
        val user = User(
            name = editText_full_name.text.toString(),
            email = editTextText_email.text.toString(),
            address = editTextText_email.text.toString(),
            phone = editTextPhone.text.toString(),
            gender = gender

        ).apply {
            sharedPreferences?.setValue(Constants.PREF_USER, this.toJsonString()).also {
                startActivity<DetailScreenActivity>()
            }

        }
    }
}