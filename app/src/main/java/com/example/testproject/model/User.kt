package com.example.testproject.model

import android.provider.ContactsContract

data class User(
   val name:String? = null,
   val email: String? =null,
   val address: String? =null,
   val phone: String? =null,
   val gender: String? =null
)
