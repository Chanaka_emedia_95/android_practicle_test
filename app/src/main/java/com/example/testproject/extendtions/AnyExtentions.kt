package com.example.testproject.extendtions

import com.google.gson.Gson

fun Any.toJsonString(): String = Gson().toJson(this@toJsonString)


inline fun <reified T : Any> String.jsonStringMapTo(): T =
    Gson().fromJson(this@jsonStringMapTo, T::class.java)
