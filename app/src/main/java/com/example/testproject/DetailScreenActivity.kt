package com.example.testproject

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.testproject.extendtions.get
import com.example.testproject.extendtions.jsonStringMapTo
import com.example.testproject.model.User
import kotlinx.android.synthetic.main.activity_detail_screen.*

class DetailScreenActivity : AppCompatActivity() {

    private val sharedPreferences: SharedPreferences? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_screen)

        val user: User? = sharedPreferences?.get<String>(Constants.PREF_USER)?.jsonStringMapTo<User>()

        Log.d("user",user.toString())
        text_address.setText(user?.address)
        text_gender.setText(user?.gender)
        text_phone.setText(user?.phone)
        text_email.setText(user?.email)
        text_name.setText(user?.name)

    }




}